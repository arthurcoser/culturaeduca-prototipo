import axios from "axios";

const findAllPublished = async () => {
  const res = await axios.get<any[]>("geoserver_layers/published");
  return res.data;
};

export default {
  findAllPublished,
};
