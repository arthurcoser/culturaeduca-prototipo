import axios from "axios";

import geoserverLayers from "./geoserver-layers.requester";

axios.defaults.baseURL = process.env.VUE_APP_URL_API || "http://localhost:3000";
axios.defaults.withCredentials = true;

export default {
  geoserverLayers,
};
