// Composables
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("@/layouts/LayoutDefault.vue"),
    children: [
      {
        path: "",
        name: "Home",
        // route level code-splitting
        // this generates a separate chunk (home.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Home.vue"),
      },
    ],
  },
  {
    path: "/maps",
    component: () => import("@/layouts/LayoutMap.vue"),
    children: [
      {
        path: "d3-sp",
        name: "MapsD3Sp",
        component: () =>
          import(/* webpackChunkName: "map" */ "@/pages/maps/MapsD3Sp.vue"),
      },
      {
        path: "leaflet",
        name: "MapsLeaflet",
        component: () =>
          import(/* webpackChunkName: "map" */ "@/pages/maps/MapsLeaflet.vue"),
      },
      {
        path: "openlayers",
        name: "MapsOpenLayers",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/maps/MapsOpenLayers.vue"
          ),
      },
      {
        path: "geoserver_layers_menu",
        name: "MapsGeoserverLayersMenu",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/maps/MapsGeoserverLayersMenu.vue"
          ),
      },
      {
        path: "leaflet_custom_menu",
        name: "MapsLeafletCustomMenu",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/maps/MapsLeafletCustomMenu.vue"
          ),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
