import * as d3 from "d3";
import { LatLng, Map } from "leaflet";

export function renderChoroplethLayer(map: Map, geoData: any, options: any) {
  if (options.propertyFilter) {
    delete geoData.bbox;
    geoData.features = geoData.features.filter(
      (el) =>
        el?.properties?.[options.propertyFilter.key] ===
        options.propertyFilter.value
    );
  }
  const colorScale = d3
    .scaleSequential()
    .domain(options?.domain || [0, 4]) // Customize the domain based on your data
    .interpolator(d3.interpolateReds); // Customize the color scheme

  const svg = d3.select(map.getPanes().overlayPane).append("svg");
  const g = svg.append("g").attr("class", "leaflet-zoom-hide");

  const transform = d3.geoTransform({ point: projectPoint });
  const path = d3.geoPath().projection(transform);

  const feature = g
    .selectAll("path")
    .data(geoData.features)
    .enter()
    .append("path")
    .attr("class", "choropleth")
    .style("fill", (d: any) => colorScale(d.properties?.[options?.valueKey]))
    .style("fill-opacity", 0.7)
    .style("stroke", "#fff")
    .style("stroke-width", 0.5);

  map.on("zoomend", reset);

  reset();

  function reset() {
    const bounds = path.bounds(geoData);
    const topLeft = bounds[0];
    const bottomRight = bounds[1];

    svg
      .attr("width", bottomRight[0] - topLeft[0])
      .attr("height", bottomRight[1] - topLeft[1])
      .style("left", topLeft[0] + "px")
      .style("top", topLeft[1] + "px");

    g.attr("transform", `translate(${-topLeft[0]}, ${-topLeft[1]})`);

    feature.attr("d", path);
  }

  function projectPoint(x: number, y: number) {
    const point = map.latLngToLayerPoint(new LatLng(y, x));
    this.stream.point(point.x, point.y);
  }

  return svg;
}
