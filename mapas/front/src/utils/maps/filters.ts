import { Marker, TileLayer } from "leaflet";

/**
 * Aplica o filtro de raio em um setor (a partir de um marcador).
 * @param {number} radius - Raio de abrangência do filtro
 * @param {TileLayer.WMS} wmsLayer - Layer a ser filtrada
 * @param {Marker} marker - Marcador a partir do qual definir o raio
 */
export default function applyRadiusFilter(radius: number, wmsLayer: TileLayer.WMS, marker: Marker) {
  const cqlFilter = `DWITHIN(geom, POINT(${marker.getLatLng().lng} ${
    marker.getLatLng().lat
  }), ${radius}, meters)`;

  wmsLayer.setParams({
    cql_filter: cqlFilter,
  } as any);
}
