-- CreateEnum
CREATE TYPE "CatalogType" AS ENUM ('GEOSPATIAL_LINE', 'GEOSPATIAL_POINT', 'GEOSPATIAL_POLYGON', 'NON_GEOSPATIAL');

-- CreateEnum
CREATE TYPE "DataIngestionFileType" AS ENUM ('CSV', 'GEOJSON', 'JSON', 'SHAPEFILE');

-- CreateEnum
CREATE TYPE "DataIngestionLogStatus" AS ENUM ('CREATED', 'FILE_UPLOAD_STARTED', 'FILE_UPLOAD_SUCCESS', 'FILE_UPLOAD_FAILURE', 'FILE_PARSING_STARTED', 'FILE_PARSING_SUCCESS', 'FILE_PARSING_FAILURE', 'DATA_LOADING_STARTED', 'DATA_LOADING_SUCCESS', 'DATA_LOADING_FAILURE');

-- CreateEnum
CREATE TYPE "DataIngestionGeoType" AS ENUM ('LAT_LNG', 'GEOJSON', 'WKT');

-- CreateTable
CREATE TABLE "catalog" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "type" "CatalogType" NOT NULL,
    "format" TEXT,
    "uri" TEXT,
    "owner" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "catalog_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "version" (
    "id" SERIAL NOT NULL,
    "catalog_id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "table_name" TEXT,
    "initial_date" DATE,
    "end_date" DATE,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "version_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "attribute" (
    "id" SERIAL NOT NULL,
    "version_id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "type" TEXT NOT NULL,
    "optional" BOOLEAN NOT NULL,
    "indexable" BOOLEAN NOT NULL,
    "unique" BOOLEAN NOT NULL,
    "sensitiveData" BOOLEAN NOT NULL,
    "order" INTEGER NOT NULL,
    "defaultValue" TEXT,
    "unit" TEXT,
    "decodeValue" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "attribute_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion" (
    "id" SERIAL NOT NULL,
    "version_id" INTEGER NOT NULL,
    "file_type" "DataIngestionFileType" NOT NULL,
    "file_path" TEXT,
    "collection_name" TEXT,
    "collection_schema" JSONB,

    CONSTRAINT "data_ingestion_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion_log" (
    "id" SERIAL NOT NULL,
    "data_ingestion_id" INTEGER NOT NULL,
    "status" "DataIngestionLogStatus" NOT NULL,
    "message" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "data_ingestion_log_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion_column" (
    "id" SERIAL NOT NULL,
    "attribute_id" INTEGER NOT NULL,
    "data_ingestion_id" INTEGER NOT NULL,
    "property_name" TEXT NOT NULL,

    CONSTRAINT "data_ingestion_column_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion_geo" (
    "id" SERIAL NOT NULL,
    "attribute_id" INTEGER NOT NULL,
    "data_ingestion_id" INTEGER NOT NULL,
    "type" "DataIngestionGeoType" NOT NULL,
    "property_name_lat" TEXT,
    "property_name_lng" TEXT,
    "property_name_wkt" TEXT,

    CONSTRAINT "data_ingestion_geo_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "catalog_name_key" ON "catalog"("name");

-- AddForeignKey
ALTER TABLE "version" ADD CONSTRAINT "version_catalog_id_fkey" FOREIGN KEY ("catalog_id") REFERENCES "catalog"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute" ADD CONSTRAINT "attribute_version_id_fkey" FOREIGN KEY ("version_id") REFERENCES "version"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion" ADD CONSTRAINT "data_ingestion_version_id_fkey" FOREIGN KEY ("version_id") REFERENCES "version"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_log" ADD CONSTRAINT "data_ingestion_log_data_ingestion_id_fkey" FOREIGN KEY ("data_ingestion_id") REFERENCES "data_ingestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_column" ADD CONSTRAINT "data_ingestion_column_attribute_id_fkey" FOREIGN KEY ("attribute_id") REFERENCES "attribute"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_column" ADD CONSTRAINT "data_ingestion_column_data_ingestion_id_fkey" FOREIGN KEY ("data_ingestion_id") REFERENCES "data_ingestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_geo" ADD CONSTRAINT "data_ingestion_geo_attribute_id_fkey" FOREIGN KEY ("attribute_id") REFERENCES "attribute"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_geo" ADD CONSTRAINT "data_ingestion_geo_data_ingestion_id_fkey" FOREIGN KEY ("data_ingestion_id") REFERENCES "data_ingestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;
