-- CreateEnum
CREATE TYPE "GeoserverLayerCategory" AS ENUM ('LIMITES_TERRITORIAIS', 'EQUIPAMENTOS_PUBLICOS_CULTURA', 'EQUIPAMENTOS_PUBLICOS_EDUCACAO');

-- CreateTable
CREATE TABLE "geoserver_layer" (
    "id" SERIAL NOT NULL,
    "catalog_id" INTEGER NOT NULL,
    "category" "GeoserverLayerCategory" NOT NULL,
    "name" TEXT NOT NULL,
    "order" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "geoserver_layer_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "geoserver_layer_catalog_id_key" ON "geoserver_layer"("catalog_id");

-- AddForeignKey
ALTER TABLE "geoserver_layer" ADD CONSTRAINT "geoserver_layer_catalog_id_fkey" FOREIGN KEY ("catalog_id") REFERENCES "catalog"("id") ON DELETE CASCADE ON UPDATE CASCADE;
