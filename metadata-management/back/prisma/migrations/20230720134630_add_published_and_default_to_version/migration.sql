-- AlterTable
ALTER TABLE "version" ADD COLUMN     "default" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "published" BOOLEAN NOT NULL DEFAULT false;
