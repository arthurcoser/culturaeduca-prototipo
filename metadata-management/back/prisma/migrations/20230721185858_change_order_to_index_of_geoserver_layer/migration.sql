/*
  Warnings:

  - You are about to drop the column `order` on the `geoserver_layer` table. All the data in the column will be lost.
  - Added the required column `index` to the `geoserver_layer` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "geoserver_layer" DROP COLUMN "order",
ADD COLUMN     "index" INTEGER NOT NULL;
