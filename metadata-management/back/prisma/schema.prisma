// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
  output   = "./generated/client"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

// MODELS

model Catalog {
  id             Int             @id @default(autoincrement())
  name           String          @unique
  description    String?         @db.Text
  type           CatalogType
  format         String?
  uri            String?
  owner          String?
  createdAt      DateTime        @default(now()) @map("created_at")
  updatedAt      DateTime        @updatedAt @map("updated_at")
  // relations
  versions       Version[]
  geoserverLayer GeoserverLayer?

  @@map("catalog")
}

enum CatalogType {
  GEOSPATIAL_LINE
  GEOSPATIAL_POINT
  GEOSPATIAL_POLYGON
  NON_GEOSPATIAL
}

model Version {
  id             Int             @id @default(autoincrement())
  catalogId      Int             @map("catalog_id")
  name           String
  description    String?         @db.Text
  tableName      String?         @map("table_name")
  tableRowsCount Int?            @map("table_rows_count")
  initialDate    DateTime?       @map("initial_date") @db.Date
  endDate        DateTime?       @map("end_date") @db.Date
  published      Boolean         @default(false)
  default        Boolean         @default(false)
  createdAt      DateTime        @default(now()) @map("created_at")
  updatedAt      DateTime        @updatedAt @map("updated_at")
  // relations
  catalog        Catalog         @relation(fields: [catalogId], references: [id], onDelete: Cascade)
  attributes     Attribute[]
  dataIngestions DataIngestion[]

  @@map("version")
}

model Attribute {
  id                   Int                   @id @default(autoincrement())
  versionId            Int                   @map("version_id")
  name                 String
  description          String?               @db.Text
  type                 String
  optional             Boolean
  indexable            Boolean
  unique               Boolean
  sensitiveData        Boolean
  order                Int
  defaultValue         String?
  unit                 String?
  decodeValue          String?
  createdAt            DateTime              @default(now()) @map("created_at")
  updatedAt            DateTime              @updatedAt @map("updated_at")
  // relations
  version              Version               @relation(fields: [versionId], references: [id], onDelete: Cascade)
  dataIngestionColumns DataIngestionColumn[]
  dataIngestionGeo     DataIngestionGeo[]

  @@map("attribute")
}

model DataIngestion {
  id                   Int                   @id @default(autoincrement())
  versionId            Int                   @map("version_id")
  fileType             DataIngestionFileType @map("file_type")
  filePath             String?               @map("file_path")
  collectionName       String?               @map("collection_name")
  collectionSchema     Json?                 @map("collection_schema")
  // relations
  version              Version               @relation(fields: [versionId], references: [id], onDelete: Cascade)
  logs                 DataIngestionLog[]
  dataIngestionColumns DataIngestionColumn[]
  dataIngestionGeo     DataIngestionGeo[]

  @@map("data_ingestion")
}

enum DataIngestionFileType {
  CSV
  GEOJSON
  JSON
  SHAPEFILE
}

model DataIngestionLog {
  id              Int                    @id @default(autoincrement())
  dataIngestionId Int                    @map("data_ingestion_id")
  status          DataIngestionLogStatus
  message         String?                @db.Text
  createdAt       DateTime               @default(now()) @map("created_at")
  updatedAt       DateTime               @updatedAt @map("updated_at")
  // relations
  dataIngestion   DataIngestion          @relation(fields: [dataIngestionId], references: [id], onDelete: Cascade)

  @@map("data_ingestion_log")
}

enum DataIngestionLogStatus {
  CREATED
  FILE_UPLOAD_STARTED
  FILE_UPLOAD_SUCCESS
  FILE_UPLOAD_FAILURE
  FILE_PARSING_STARTED
  FILE_PARSING_SUCCESS
  FILE_PARSING_FAILURE
  DATA_LOADING_STARTED
  DATA_LOADING_SUCCESS
  DATA_LOADING_FAILURE
}

model DataIngestionColumn {
  id              Int           @id @default(autoincrement())
  attributeId     Int           @map("attribute_id")
  dataIngestionId Int           @map("data_ingestion_id")
  propertyName    String        @map("property_name")
  // relations
  attribute       Attribute     @relation(fields: [attributeId], references: [id], onDelete: Cascade)
  dataIngestion   DataIngestion @relation(fields: [dataIngestionId], references: [id], onDelete: Cascade)

  @@map("data_ingestion_column")
}

model DataIngestionGeo {
  id              Int                  @id @default(autoincrement())
  attributeId     Int                  @map("attribute_id")
  dataIngestionId Int                  @map("data_ingestion_id")
  type            DataIngestionGeoType
  propertyNameLat String?              @map("property_name_lat")
  propertyNameLng String?              @map("property_name_lng")
  propertyNameWkt String?              @map("property_name_wkt")
  // relations
  attribute       Attribute            @relation(fields: [attributeId], references: [id], onDelete: Cascade)
  dataIngestion   DataIngestion        @relation(fields: [dataIngestionId], references: [id], onDelete: Cascade)

  @@map("data_ingestion_geo")
}

enum DataIngestionGeoType {
  LAT_LNG
  GEOJSON
  WKT
}

model GeoserverLayer {
  id        Int                    @id @default(autoincrement())
  catalogId Int                    @unique @map("catalog_id")
  category  GeoserverLayerCategory
  name      String
  index     Int
  createdAt DateTime               @default(now()) @map("created_at")
  updatedAt DateTime               @updatedAt @map("updated_at")
  // relations
  catalog   Catalog                @relation(fields: [catalogId], references: [id], onDelete: Cascade)

  @@map("geoserver_layer")
}

enum GeoserverLayerCategory {
  LIMITES_TERRITORIAIS
  EQUIPAMENTOS_PUBLICOS_CULTURA
  EQUIPAMENTOS_PUBLICOS_EDUCACAO
}
