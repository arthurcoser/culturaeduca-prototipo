import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { AttributesModule } from './attributes/attributes.module';
import { CatalogsModule } from './catalogs/catalogs.module';
import { DataIngestionsModule } from './data-ingestions/data-ingestions.module';
import { VersionsModule } from './versions/versions.module';
import { EventsModule } from './events/events.module';
import { GeoserverLayersModule } from './geoserver-layers/geoserver-layers.module';
import configuration from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      load: [configuration],
    }),
    EventsModule,
    // models
    AttributesModule,
    CatalogsModule,
    DataIngestionsModule,
    VersionsModule,
    GeoserverLayersModule,
  ],
  providers: [],
  exports: [],
})
export class AppModule {}
