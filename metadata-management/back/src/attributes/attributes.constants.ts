export enum TypeAttributeEnum {
  STRING = 'STRING',
  INT = 'INT',
  LONG = 'LONG',
  FLOAT = 'FLOAT',
  DOUBLE = 'DOUBLE',
  BOOLEAN = 'BOOLEAN',
  BYTES = 'BYTES',
  GEOMETRY = 'GEOMETRY',
}

export const typesAttributesDescriptions = {
  [TypeAttributeEnum.BOOLEAN]: 'a binary value',
  [TypeAttributeEnum.INT]: '32-bit signed integer',
  [TypeAttributeEnum.LONG]: '64-bit signed integer',
  [TypeAttributeEnum.FLOAT]:
    'single precision (32-bit) IEEE 754 floating-point number',
  [TypeAttributeEnum.DOUBLE]:
    'double precision (64-bit) IEEE 754 floating-point number',
  [TypeAttributeEnum.BYTES]: 'sequence of 8-bit unsigned bytes',
  [TypeAttributeEnum.STRING]: 'unicode character sequence',
  [TypeAttributeEnum.GEOMETRY]: 'geospatial data',
};

export const typesAttributesKnexColumnFunction = {
  [TypeAttributeEnum.BOOLEAN]: 'boolean',
  [TypeAttributeEnum.INT]: 'integer',
  [TypeAttributeEnum.LONG]: 'bigInteger',
  [TypeAttributeEnum.FLOAT]: 'float',
  [TypeAttributeEnum.DOUBLE]: 'double',
  [TypeAttributeEnum.BYTES]: 'binary',
  [TypeAttributeEnum.STRING]: 'string',
  [TypeAttributeEnum.GEOMETRY]: 'geometry',
};
