import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { AttributesService } from './attributes.service';
import { UpdateAttributeDto } from './dto';

@Controller('attributes')
export class AttributesController {
  constructor(private readonly attributesService: AttributesService) {}

  // Get

  @Get(':id/bases_geojson/:basesDataId')
  getBasesGeojson(
    @Param('id', ParseIntPipe) id: number,
    @Param('basesDataId', ParseIntPipe) basesDataId: number,
  ) {
    return this.attributesService.getBasesGeojson(id, basesDataId);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.attributesService.findOne(id);
  }

  // Patch

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateAttributeDto: UpdateAttributeDto,
  ) {
    return this.attributesService.update(id, updateAttributeDto);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.attributesService.remove(id);
  }
}
