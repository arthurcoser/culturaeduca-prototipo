import { Module } from '@nestjs/common';

import { KnexProviderBases } from '@/knex.provider';
import { PrismaService } from '@/prisma.service';
import { AttributesController } from './attributes.controller';
import { AttributesService } from './attributes.service';

@Module({
  controllers: [AttributesController],
  providers: [AttributesService, PrismaService, KnexProviderBases],
})
export class AttributesModule {}
