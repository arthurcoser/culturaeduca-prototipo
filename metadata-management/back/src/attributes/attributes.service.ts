import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Knex } from 'knex';

import { Attribute } from '@/../prisma/generated/client';
import { KnexConnectionBases } from '@/knex.provider';
import { PrismaService } from '@/prisma.service';
import { UpdateAttributeDto } from './dto';
import {
  TypeAttributeEnum,
  typesAttributesKnexColumnFunction,
} from './attributes.constants';

@Injectable()
export class AttributesService {
  constructor(
    @Inject(KnexConnectionBases) private readonly knexBases: Knex,
    private readonly prisma: PrismaService,
  ) {}

  async findOne(id: number): Promise<Attribute> {
    const attribute = await this.prisma.attribute.findUnique({
      where: { id },
    });
    if (!attribute) {
      throw new NotFoundException('Attribute not found!');
    }
    return attribute;
  }

  async update(id: number, dto: UpdateAttributeDto): Promise<Attribute> {
    // find attribute
    const oldAttribute = await this.prisma.attribute.findUnique({
      where: { id },
      include: { version: true },
    });
    if (!oldAttribute) {
      throw new NotFoundException('Attribute not found!');
    }
    // update attribute
    return this.prisma.$transaction(async (trxPrisma) => {
      // data for update
      const dataNewAttribute = {
        ...(dto?.name !== undefined ? { name: dto.name?.trim() || null } : {}),
        ...(dto?.description !== undefined
          ? { description: dto.description?.trim() || null }
          : {}),
        ...(dto?.type !== undefined ? { type: dto.type?.trim() || null } : {}),
        ...(dto?.optional !== undefined ? { optional: dto.optional } : {}),
        ...(dto?.indexable !== undefined ? { indexable: dto.indexable } : {}),
        ...(dto?.unique !== undefined ? { unique: dto.unique } : {}),
        ...(dto?.sensitiveData !== undefined
          ? { sensitiveData: dto.sensitiveData }
          : {}),
        ...(dto?.defaultValue !== undefined
          ? { defaultValue: dto.defaultValue?.trim() || null }
          : {}),
        ...(dto?.unit !== undefined ? { unit: dto.unit?.trim() || null } : {}),
        ...(dto?.decodeValue !== undefined
          ? { decodeValue: dto.decodeValue?.trim() || null }
          : {}),
      };
      // update attribute
      const newAttribute = await trxPrisma.attribute.update({
        where: { id },
        data: dataNewAttribute,
      });
      // compare changes to update column in bases table
      await this.knexBases.transaction(async (trxKnex) => {
        const tableName = oldAttribute.version.tableName;
        await trxKnex.schema.alterTable(tableName, (table) => {
          // rename column (change attribute name)
          if (oldAttribute.name !== newAttribute.name) {
            table.renameColumn(oldAttribute.name, newAttribute.name);
          }
          // type and optional
          if (oldAttribute.type !== newAttribute.type) {
            const knexColumnFunction =
              typesAttributesKnexColumnFunction?.[newAttribute.type] || 'text';
            const knexNullableFunction = newAttribute.optional
              ? 'nullable'
              : 'notNullable';
            table[knexColumnFunction](newAttribute.name)
              [knexNullableFunction]()
              .alter();
          }
          // index
          if (oldAttribute.indexable !== newAttribute.indexable) {
            if (newAttribute.indexable === true) {
              table.index(newAttribute.name);
            } else {
              table.dropIndex(newAttribute.name);
            }
          }
          // unique
          if (oldAttribute.unique !== newAttribute.unique) {
            if (newAttribute.unique === true) {
              table.unique([newAttribute.name]);
            } else {
              table.dropUnique([newAttribute.name]);
            }
          }
        });
      });
      return newAttribute;
    });
  }

  async remove(id: number): Promise<Attribute> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // delete attribute
      const attribute = await trxPrisma.attribute.delete({
        where: { id },
        include: { version: true },
      });
      if (!attribute) {
        throw new NotFoundException('Attribute not found!');
      }
      // drop column
      await this.knexBases.transaction(async (trxKnex) => {
        const tableName = attribute.version.tableName;
        await trxKnex.schema.alterTable(tableName, (table) => {
          table.dropColumn(attribute.name);
        });
      });
      return attribute;
    });
  }

  async getBasesGeojson(id: number, basesDataId: number) {
    // find attribute
    const attribute = await this.prisma.attribute.findUnique({
      where: { id },
      include: { version: true },
    });
    if (!attribute) {
      throw new NotFoundException('Attribute not found!');
    }
    if (attribute.type !== TypeAttributeEnum.GEOMETRY) {
      throw new NotFoundException('Attribute is not geospatial!');
    }
    // get column as geojson
    const results = await this.knexBases
      .table(attribute.version.tableName)
      .select(
        this.knexBases.raw(
          `ST_AsGeoJSON("${attribute.name}"::geography)::jsonb as "${attribute.name}"`,
        ),
      )
      .where({ _id: basesDataId });
    return results?.[0]?.[attribute.name];
  }
}
