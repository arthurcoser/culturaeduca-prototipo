import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class CreateAttributeDto {
  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  description: string | null;

  @IsString()
  type: string;

  @IsBoolean()
  optional: boolean;

  @IsBoolean()
  indexable: boolean;

  @IsBoolean()
  unique: boolean;

  @IsBoolean()
  sensitiveData: boolean;

  @IsString()
  @IsOptional()
  defaultValue: string | null;

  @IsString()
  @IsOptional()
  unit: string | null;

  @IsString()
  @IsOptional()
  decodeValue: string | null;
}
