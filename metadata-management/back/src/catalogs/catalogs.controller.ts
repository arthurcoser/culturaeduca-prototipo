import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { CatalogsService } from './catalogs.service';
import { CreateCatalogDto, UpdateCatalogDto } from './dto';
import { CreateVersionDto } from '@/versions/dto';
import { CreateGeoserverLayerDto } from '@/geoserver-layers/dto';

@Controller('catalogs')
export class CatalogsController {
  constructor(private readonly catalogsService: CatalogsService) {}

  // Get

  @Get(':id/versions')
  findVersions(@Param('id', ParseIntPipe) id: number) {
    return this.catalogsService.findVersions(id);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.catalogsService.findOne(id);
  }

  @Get()
  findAll() {
    return this.catalogsService.findAll();
  }

  // Post

  @Post(':id/versions')
  createVersion(
    @Param('id', ParseIntPipe) id: number,
    @Body() createVersionDto: CreateVersionDto,
  ) {
    return this.catalogsService.createVersion(id, createVersionDto);
  }

  @Post(':id/geoserver_layers')
  createGeoserverLayer(
    @Param('id', ParseIntPipe) id: number,
    @Body() createGeoserverLayerDto: CreateGeoserverLayerDto,
  ) {
    return this.catalogsService.createGeoserverLayer(
      id,
      createGeoserverLayerDto,
    );
  }

  @Post()
  create(@Body() createCatalogDto: CreateCatalogDto) {
    return this.catalogsService.create(createCatalogDto);
  }

  // Patch

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateCatalogDto: UpdateCatalogDto,
  ) {
    return this.catalogsService.update(id, updateCatalogDto);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.catalogsService.remove(id);
  }
}
