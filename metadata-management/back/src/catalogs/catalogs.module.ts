import { Module } from '@nestjs/common';

import { CatalogsController } from './catalogs.controller';
import { CatalogsService } from './catalogs.service';
import { PrismaService } from '@/prisma.service';
import { KnexProviderBases } from '@/knex.provider';

@Module({
  controllers: [CatalogsController],
  providers: [CatalogsService, PrismaService, KnexProviderBases],
})
export class CatalogsModule {}
