import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Knex } from 'knex';

import {
  Catalog,
  CatalogType,
  GeoserverLayer,
  Version,
} from '@/../prisma/generated/client';
import { CreateVersionDto } from '@/versions/dto';
import { PrismaService } from '@/prisma.service';
import { KnexConnectionBases } from '@/knex.provider';
import { CreateCatalogDto, UpdateCatalogDto } from './dto';
import { CreateGeoserverLayerDto } from '@/geoserver-layers/dto';

@Injectable()
export class CatalogsService {
  constructor(
    @Inject(KnexConnectionBases) private readonly knexBases: Knex,
    private readonly prisma: PrismaService,
  ) {}

  async create(dto: CreateCatalogDto): Promise<Catalog> {
    const catalog = await this.prisma.catalog.create({
      data: {
        name: dto?.name?.trim() || null,
        description: dto?.description?.trim() || null,
        type: dto.type || null,
        format: dto?.format?.trim() || null,
        uri: dto?.uri?.trim() || null,
        owner: dto?.owner?.trim() || null,
      },
    });
    return catalog;
  }

  async createGeoserverLayer(
    id: number,
    dto: CreateGeoserverLayerDto,
  ): Promise<GeoserverLayer> {
    // find catalog
    const catalog = await this.prisma.catalog.findUnique({
      where: { id },
    });
    if (!catalog) {
      throw new NotFoundException('Catalog not found!');
    }
    if (catalog.type === CatalogType.NON_GEOSPATIAL) {
      throw new BadRequestException('Catalog must be geospatial!');
    }
    // get new INDEX
    const newCategory = dto?.category || null;
    const layerSameCategoryMaxIndex = await this.prisma.geoserverLayer.groupBy({
      by: ['category'],
      where: {
        category: newCategory,
      },
      _max: {
        index: true,
      },
    });
    const newIndex = (layerSameCategoryMaxIndex?.[0]?._max?.index || 0) + 1;
    // create geoserver layer
    const geoserverLayer = await this.prisma.geoserverLayer.create({
      data: {
        catalogId: catalog.id,
        category: newCategory,
        name: dto?.name?.trim() || null,
        index: newIndex,
      },
    });
    return geoserverLayer;
  }

  async createVersion(id: number, dto: CreateVersionDto): Promise<Version> {
    // find catalog
    const catalog = await this.prisma.catalog.findUnique({
      where: { id },
    });
    if (!catalog) {
      throw new NotFoundException('Catalog not found!');
    }
    return this.prisma.$transaction(async (trxPrisma) => {
      // create version
      let version = await trxPrisma.version.create({
        data: {
          catalogId: catalog.id,
          name: dto?.name?.trim() || null,
          description: dto?.description?.trim() || null,
          tableName: null,
          tableRowsCount: 0,
          initialDate: dto?.initialDate?.trim() || null,
          endDate: dto?.endDate?.trim() || null,
          published: false,
          default: false,
        },
      });
      // create bases table
      const tableName = `catalog_${catalog.id}_version_${version.id}`;
      await this.knexBases.transaction(async (trxKnex) => {
        if (await trxKnex.schema.hasTable(tableName)) {
          await trxKnex.schema.dropTable(tableName);
        }
        await trxKnex.schema.createTable(tableName, (table) => {
          table.bigIncrements('_id');
          table.integer('_data_ingestion_id').notNullable();
          table
            .timestamp('_created_at', { useTz: true })
            .defaultTo(trxKnex.fn.now());
          table
            .timestamp('_updated_at', { useTz: true })
            .defaultTo(trxKnex.fn.now());
        });
      });
      // update version
      version = await trxPrisma.version.update({
        where: { id: version.id },
        data: { tableName },
      });
      return version;
    });
  }

  async findAll(): Promise<Catalog[]> {
    const catalogs = await this.prisma.catalog.findMany();
    return catalogs;
  }

  async findOne(id: number): Promise<Catalog> {
    const catalog = await this.prisma.catalog.findUnique({
      where: { id },
    });
    if (!catalog) {
      throw new NotFoundException('Catalog not found!');
    }
    return catalog;
  }

  async findVersions(id: number): Promise<Version[]> {
    const catalog = await this.prisma.catalog.findUnique({
      where: { id },
      include: {
        versions: {
          orderBy: {
            id: 'asc',
          },
        },
      },
    });
    if (!catalog) {
      throw new NotFoundException('Catalog not found!');
    }
    return catalog.versions;
  }

  async update(id: number, dto: UpdateCatalogDto): Promise<Catalog> {
    const catalog = await this.prisma.catalog.update({
      where: { id },
      data: {
        ...(dto?.name !== undefined ? { name: dto.name?.trim() || null } : {}),
        ...(dto?.description !== undefined
          ? { description: dto.description?.trim() || null }
          : {}),
        ...(dto?.type !== undefined ? { type: dto.type } : {}),
        ...(dto?.format !== undefined
          ? { format: dto.format?.trim() || null }
          : {}),
        ...(dto?.uri !== undefined ? { uri: dto.uri?.trim() || null } : {}),
        ...(dto?.owner !== undefined
          ? { owner: dto.owner?.trim() || null }
          : {}),
      },
    });
    if (!catalog) {
      throw new NotFoundException('Catalog not found!');
    }
    return catalog;
  }

  async remove(id: number): Promise<Catalog> {
    const catalog = await this.prisma.catalog.delete({
      where: { id },
    });
    if (!catalog) {
      throw new NotFoundException('Catalog not found!');
    }
    return catalog;
  }
}
