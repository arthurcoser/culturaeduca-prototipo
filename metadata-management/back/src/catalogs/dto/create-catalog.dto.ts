import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { CatalogType } from '@/../prisma/generated/client';

export class CreateCatalogDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsEnum(CatalogType)
  type: CatalogType;

  @IsOptional()
  @IsString()
  format: string | null;

  @IsOptional()
  @IsString()
  uri: string | null;

  @IsOptional()
  @IsString()
  owner: string | null;
}
