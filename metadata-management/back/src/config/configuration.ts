import 'dotenv/config';
import { Knex } from 'knex';
import { MinioModuleConfig } from '@/minio/config.interface';
import { MongooseModuleFactoryOptions } from '@nestjs/mongoose';

export default () => ({
  nodeEnv: process.env.NODE_ENV || 'development',
  appPort: Number(process.env.PORT) || 3000,
  baseUrlFront: process.env.BASE_URL_FRONT || 'http://localhost:8081',
  baseUrlMapas: process.env.BASE_URL_MAPAS || 'http://localhost:8080',
  multer: {
    dest: process.env.MULTER_DEST || './static/tmp/',
  },
  minio: {
    client: {
      endPoint: process.env.MINIO_END_POINT || 'localhost',
      port: Number(process.env.MINIO_PORT) || 9000,
      useSSL: process.env.MINIO_USE_SSL === 'true' || false,
      accessKey: process.env.MINIO_ACCESS_KEY,
      secretKey: process.env.MINIO_SECRET_KEY,
    } as MinioModuleConfig,
  },
  mongoose: {
    uri: process.env.MONGODB_URL,
  } as MongooseModuleFactoryOptions,
  knex: {
    bases: {
      client: 'pg',
      connection: process.env.DATABASE_BASES_URL,
    } as Knex.Config,
  },
});
