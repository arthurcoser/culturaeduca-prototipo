import { ConfigService } from '@nestjs/config';
import { MinioModule } from '@/minio/minio.module';
import { MinioModuleConfig } from '@/minio/config.interface';
import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';

import { PrismaService } from '@/prisma.service';
import { DataIngestionsService } from './data-ingestions.service';
import { DataIngestionsController } from './data-ingestions.controller';
import { KnexProviderBases } from '@/knex.provider';
import { EventsGateway } from '@/events/events.gateway';
import { EventsModule } from '@/events/events.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        ...configService.get<MongooseModuleFactoryOptions>('mongoose'),
      }),
      inject: [ConfigService],
    }),
    MinioModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        ...configService.get<MinioModuleConfig>('minio.client'),
      }),
      inject: [ConfigService],
    }),
    MulterModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        ...configService.get<MulterOptions>('multer'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [DataIngestionsController],
  providers: [
    DataIngestionsService,
    EventsGateway,
    KnexProviderBases,
    PrismaService,
  ],
})
export class DataIngestionsModule {}
