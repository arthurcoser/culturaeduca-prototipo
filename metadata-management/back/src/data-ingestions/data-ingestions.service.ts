import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { join, extname } from 'path';
import { Knex } from 'knex';
import { Schema, createSchema, mergeSchemas } from 'genson-js';
import * as fs from 'fs/promises';
import * as shapefile from 'shapefile';
import * as extract from 'extract-zip';
import * as mongoose from 'mongoose';
import * as csvParser from 'csv-parser';
import { createReadStream } from 'fs';
import { decode } from 'iconv-lite';

import { MinioService } from '@/minio/minio.service';
import { PrismaService } from '@/prisma.service';
import { KnexConnectionBases } from '@/knex.provider';
import {
  Attribute,
  DataIngestion,
  DataIngestionColumn,
  DataIngestionFileType,
  DataIngestionGeo,
  DataIngestionGeoType,
  DataIngestionLog,
  DataIngestionLogStatus,
  Version,
} from '@/../prisma/generated/client';
import {
  GetCollectionDataIngestionQueryDto,
  MapAttributesDataIngestionDto,
} from './dto';
import { BucketItemStat } from 'minio';
import { EventsGateway } from '@/events/events.gateway';

@Injectable()
export class DataIngestionsService {
  constructor(
    @Inject(KnexConnectionBases) private readonly knexBases: Knex,
    @InjectConnection() private mongooseConnection: mongoose.Connection,
    private readonly minioService: MinioService,
    private readonly prisma: PrismaService,
    private readonly eventsGateway: EventsGateway,
  ) {}

  async findCurrentLog(id: number): Promise<DataIngestionLog> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: {
          orderBy: {
            createdAt: 'desc',
          },
        },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // return current log
    return dataIngestion.logs?.[0] || null;
  }

  async findLogs(id: number): Promise<DataIngestionLog[]> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: {
          orderBy: {
            createdAt: 'asc',
          },
        },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // return current log
    return dataIngestion.logs;
  }

  async findOne(id: number): Promise<DataIngestion> {
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    return dataIngestion;
  }

  async getCollection(
    id: number,
    queryDto: GetCollectionDataIngestionQueryDto,
  ): Promise<{ total: number; results: any[] }> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    if (!dataIngestion.collectionName) {
      throw new NotFoundException('Data ingestion does not have a collection!');
    }
    // create mongoose model
    const model = await this.mongooseConnection.model(
      dataIngestion.collectionName,
      new mongoose.Schema({ any: {} }),
    );
    const page = queryDto?.page || 1;
    const perPage = queryDto?.perPage || 10;
    const limit = perPage;
    const skip = perPage * (page - 1);
    // query data
    const results = await model
      .find()
      .select(['type', 'properties', 'geometry.type'])
      .limit(limit)
      .skip(skip)
      .exec();
    // conut documents
    const total = await model.countDocuments();
    // delete mongoose model
    await this.mongooseConnection.deleteModel(dataIngestion.collectionName);
    return { total, results };
  }

  async getFileInfo(id: number): Promise<BucketItemStat> {
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    if (!dataIngestion.filePath) {
      return null;
    }
    return this.minioService.statObject(dataIngestion.filePath);
  }

  async loadData(id: number): Promise<void> {
    // get data ingestion with version and attributes
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: {
          orderBy: { createdAt: 'desc' },
        },
        version: {
          include: {
            attributes: {
              include: {
                dataIngestionColumns: true,
                dataIngestionGeo: true,
              },
            },
          },
        },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // check status (log is ordered desc)
    const currentLog = dataIngestion.logs?.[0];
    if (
      currentLog?.status !== DataIngestionLogStatus.FILE_PARSING_SUCCESS &&
      currentLog?.status !== DataIngestionLogStatus.DATA_LOADING_FAILURE
    ) {
      throw new BadRequestException('Invalid data ingestion status!');
    }
    // check if all attributes are mapped
    const attributesMapped = dataIngestion.version.attributes.every(
      (attribute) =>
        attribute.dataIngestionColumns.length +
          attribute.dataIngestionGeo.length >=
        1,
    );
    if (!attributesMapped) {
      throw new BadRequestException('All attributes must be mapped!');
    }
    const newLog = await this.prisma.dataIngestionLog.create({
      data: {
        dataIngestionId: dataIngestion.id,
        status: DataIngestionLogStatus.DATA_LOADING_STARTED,
      },
    });
    this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    this.loadDataHelper(dataIngestion);
  }

  async loadDataHelper(
    dataIngestion: DataIngestion & {
      logs: DataIngestionLog[];
      version: Version & {
        attributes: Array<
          Attribute & {
            dataIngestionColumns: DataIngestionColumn[];
            dataIngestionGeo: DataIngestionGeo[];
          }
        >;
      };
    },
  ): Promise<void> {
    // mappers
    const propertiesMapper = [];
    const geoJsonMapper = [];
    const latLngMapper = [];
    const wktMapper = [];
    dataIngestion.version.attributes.forEach((attribute) => {
      attribute.dataIngestionColumns.forEach((column) => {
        propertiesMapper.push({
          attribute: attribute.name,
          property: column.propertyName,
        });
      });
      attribute.dataIngestionGeo.forEach((geo) => {
        switch (geo.type) {
          case DataIngestionGeoType.GEOJSON:
            geoJsonMapper.push({
              attribute: attribute.name,
            });
            break;
          case DataIngestionGeoType.LAT_LNG:
            latLngMapper.push({
              attribute: attribute.name,
              propertyLat: geo.propertyNameLat,
              propertyLng: geo.propertyNameLng,
            });
            break;
          case DataIngestionGeoType.WKT:
            wktMapper.push({
              attribute: attribute.name,
              propertyWkt: geo.propertyNameWkt,
            });
          default:
            break;
        }
      });
    });
    // create mongoose model
    const model = await this.mongooseConnection.model(
      dataIngestion.collectionName,
      new mongoose.Schema({ any: {} }),
    );
    // load data
    const tableName = dataIngestion.version.tableName;
    try {
      await this.knexBases.transaction(async (trxKnex) => {
        const total = await model.countDocuments();
        const limit = 1000;
        for (let skip = 0; skip < Math.ceil(total / limit); skip += 1) {
          const results = await model
            .find()
            .select(['properties', 'geometry'])
            .limit(limit)
            .skip(skip * limit)
            .exec();

          const dataInsert = results.map((result) => {
            // data starts with dataIngestionId
            const data = {
              _data_ingestion_id: dataIngestion.id,
            };
            // populate properties
            for (let i = 0; i < propertiesMapper.length; i++) {
              data[propertiesMapper[i].attribute] =
                result['_doc']?.properties?.[propertiesMapper[i].property];
            }
            // populate geometries
            for (let i = 0; i < geoJsonMapper.length; i++) {
              data[geoJsonMapper[i].attribute] = result['_doc']?.geometry;
            }
            for (let i = 0; i < latLngMapper.length; i++) {
              const lat =
                result['_doc']?.properties?.[latLngMapper[i].propertyLat];
              const lng =
                result['_doc']?.properties?.[latLngMapper[i].propertyLng];
              data[latLngMapper[i].attribute] = trxKnex.raw('ST_Point(?, ?)', [
                lng,
                lat,
              ]);
            }
            for (let i = 0; i < wktMapper.length; i++) {
              const wkt =
                result['_doc']?.properties?.[wktMapper[i].propertyWkt];
              data[wktMapper[i].attribute] = trxKnex.raw('ST_GeomFromText(?)', [
                wkt,
              ]);
            }
            return data;
          });
          // load data
          await trxKnex.insert(dataInsert).into(tableName);
        }
      });
      // update status
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.DATA_LOADING_SUCCESS,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      // update data count
      const [{ count }] = await this.knexBases(tableName).count();
      await this.prisma.version.update({
        where: { id: dataIngestion.versionId },
        data: { tableRowsCount: Number(count) },
      });
    } catch (err) {
      // update status
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.DATA_LOADING_FAILURE,
          message: err.message,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
    // delete mongoose model
    await this.mongooseConnection.deleteModel(dataIngestion.collectionName);
  }

  async mapAttributes(
    id: number,
    dto: MapAttributesDataIngestionDto,
  ): Promise<void> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // map attributes
    return this.prisma.$transaction(async (trxPrisma) => {
      // reset data ingestion columns
      await trxPrisma.dataIngestionColumn.deleteMany({
        where: { id: dataIngestion.id },
      });
      await trxPrisma.dataIngestionColumn.createMany({
        data: (dto.columns || []).map((item) => ({
          dataIngestionId: dataIngestion.id,
          attributeId: item.attributeId,
          propertyName: item.propertyName,
        })),
      });
      // reset data ingestion geo
      await trxPrisma.dataIngestionGeo.deleteMany({
        where: { id: dataIngestion.id },
      });
      await trxPrisma.dataIngestionGeo.createMany({
        data: (dto.geo || []).map((item) => ({
          dataIngestionId: dataIngestion.id,
          attributeId: item.attributeId,
          type: item.type,
          propertyNameLat:
            item.type === DataIngestionGeoType.LAT_LNG
              ? item.propertyNameLat
              : null,
          propertyNameLng:
            item.type === DataIngestionGeoType.LAT_LNG
              ? item.propertyNameLng
              : null,
          propertyNameWkt:
            item.type === DataIngestionGeoType.WKT
              ? item.propertyNameWkt
              : null,
        })),
      });
    });
  }

  async parseFile(id: number): Promise<void> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: {
          orderBy: { createdAt: 'desc' },
        },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    if (!dataIngestion.filePath) {
      throw new NotFoundException('No file uploaded!');
    }
    // check status (log is ordered desc)
    const currentLog = dataIngestion.logs?.[0];
    if (
      currentLog?.status !== DataIngestionLogStatus.FILE_UPLOAD_SUCCESS &&
      currentLog?.status !== DataIngestionLogStatus.FILE_PARSING_FAILURE
    ) {
      throw new BadRequestException('Invalid data ingestion status!');
    }
    // create new log
    const newLog = await this.prisma.dataIngestionLog.create({
      data: {
        dataIngestionId: dataIngestion.id,
        status: DataIngestionLogStatus.FILE_PARSING_STARTED,
      },
    });
    this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    // choose correct function
    switch (dataIngestion.fileType) {
      case DataIngestionFileType.SHAPEFILE:
        this.parseFileHelperShapefile(dataIngestion);
        return;
      case DataIngestionFileType.CSV:
        this.parseFileHelperCsv(dataIngestion);
        return;
      case DataIngestionFileType.GEOJSON:
      case DataIngestionFileType.JSON:
      default:
        const errorLog = await this.prisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_PARSING_FAILURE,
            message: 'Not implemented yet',
          },
        });
        this.eventsGateway.server.emit('dataIngestionLog.created', errorLog);
        throw new NotImplementedException();
    }
  }

  async parseFileHelperCsv(dataIngestion: DataIngestion): Promise<void> {
    // download file to tmp
    const fileTmpPath = await this.minioService.fGetObject(
      dataIngestion.filePath,
    );
    const csvFilePath = join(__dirname, '..', '..', fileTmpPath);
    try {
      let collectionSchema: Schema;

      const collectionName = dataIngestion.collectionName;
      // drop collection to clear all data
      await this.mongooseConnection.dropCollection(collectionName);
      // create and get collection
      const collection = await this.mongooseConnection.collection(
        collectionName,
      );

      // read csv and insert one by one
      await new Promise<void>((resolve, reject) => {
        createReadStream(csvFilePath)
          .pipe(csvParser())
          .on('data', async (data) => {
            const resultValue = { properties: data };
            // infer schema and merge with current one
            collectionSchema = mergeSchemas([
              ...(collectionSchema ? [collectionSchema] : []),
              createSchema(resultValue),
            ]);
            // insert value in the collection
            await collection.insertOne(resultValue);
          })
          .on('end', () => {
            resolve();
            return;
          })
          .on('error', reject);
      });

      await this.prisma.$transaction(async (trxPrisma) => {
        // update collection schema
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { collectionSchema },
        });
        // create log success
        const newLog = await trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_PARSING_SUCCESS,
          },
        });
        this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      });
    } catch (err) {
      // create log failure
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_PARSING_FAILURE,
          message: err.message,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }

  async parseFileHelperShapefile(dataIngestion: DataIngestion): Promise<void> {
    // download file to tmp
    const fileTmpPath = await this.minioService.fGetObject(
      dataIngestion.filePath,
    );

    // extract zip and get shp
    const targetDir = join(__dirname, '..', '..', `${fileTmpPath}_extract`);
    await extract(fileTmpPath, { dir: targetDir });
    const filenames = await fs.readdir(targetDir);
    const shpFile = filenames.find((filename) => extname(filename) === '.shp');
    if (!shpFile) {
      throw new BadRequestException('Shapefile not found!');
    }
    const shpFilePath = join(targetDir, shpFile);

    try {
      let collectionSchema: Schema;

      const collectionName = dataIngestion.collectionName;
      // drop collection to clear all data
      await this.mongooseConnection.dropCollection(collectionName);
      // create and get collection
      const collection = await this.mongooseConnection.collection(
        collectionName,
      );

      // read shapefile and insert one by one
      await new Promise<void>((resolve, reject) => {
        shapefile
          .open(shpFilePath)
          .then((source) =>
            source.read().then(async function log(result) {
              // resolve promise
              if (result.done) {
                resolve();
                return;
              }
              if (!!result.value) {
                // infer schema and merge with current one
                collectionSchema = mergeSchemas([
                  ...(collectionSchema ? [collectionSchema] : []),
                  createSchema(result.value),
                ]);
                // insert value in the collection
                for (const key in result.value.properties) {
                  if (typeof result.value.properties[key] === 'string') {
                    result.value.properties[key] = decode(
                      Buffer.from(result.value.properties[key], 'latin1'),
                      'utf8',
                    );
                  }
                }
                await collection.insertOne(result.value);
              }
              // continue reading
              return source.read().then(log);
            }),
          )
          .catch(reject);
      });

      await this.prisma.$transaction(async (trxPrisma) => {
        // update collection schema
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { collectionSchema },
        });
        // create log success
        const newLog = await trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_PARSING_SUCCESS,
          },
        });
        this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      });
    } catch (err) {
      // create log failure
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_PARSING_FAILURE,
          message: err.message,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }

  async remove(id: number): Promise<DataIngestion> {
    return this.prisma.$transaction(async (trxPrisma) => {
      const dataIngestion = await trxPrisma.dataIngestion.delete({
        where: { id },
        include: {
          version: true,
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      await this.mongooseConnection.dropCollection(
        dataIngestion.collectionName,
      );
      await this.knexBases.transaction(async (trxKnex) => {
        await trxKnex
          .table(dataIngestion.version.tableName)
          .delete()
          .where({ _data_ingestion_id: dataIngestion.id });
      });
      return dataIngestion;
    });
  }

  async removeCurrentLog(id: number): Promise<DataIngestionLog> {
    // find current log
    const currentLog = await this.prisma.dataIngestionLog.findFirst({
      where: { dataIngestionId: id },
      orderBy: { createdAt: 'desc' },
    });
    if (!currentLog) {
      throw new NotFoundException('Current data ingestion log not found!');
    }
    // can't remove CREATED logs
    if (currentLog.status === DataIngestionLogStatus.CREATED) {
      throw new BadRequestException(
        'Current data ingestion log cannot be removed!',
      );
    }
    // TODO: undo data associated to the status
    // remove log
    const removedLog = await this.prisma.dataIngestionLog.delete({
      where: { id: currentLog.id },
    });
    this.eventsGateway.server.emit('dataIngestionLog.removed', removedLog);
    return removedLog;
  }

  async uploadFile(id: number, file: Express.Multer.File): Promise<void> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: {
          orderBy: { createdAt: 'desc' },
        },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // check status (log is ordered desc)
    const currentLog = dataIngestion.logs?.[0];
    if (
      currentLog?.status !== DataIngestionLogStatus.CREATED &&
      currentLog?.status !== DataIngestionLogStatus.FILE_UPLOAD_FAILURE
    ) {
      throw new BadRequestException('Invalid data ingestion status!');
    }
    const newLog = await this.prisma.dataIngestionLog.create({
      data: {
        dataIngestionId: dataIngestion.id,
        status: DataIngestionLogStatus.FILE_UPLOAD_STARTED,
      },
    });
    this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    this.uploadFileHelper(dataIngestion, file);
  }

  async uploadFileHelper(
    dataIngestion: DataIngestion,
    file: Express.Multer.File,
  ): Promise<void> {
    // upload to MinIo
    const objectName = `versions/${dataIngestion.versionId}/data_ingestions/${dataIngestion.id}/raw`;
    try {
      await this.minioService.fPutObject(objectName, file);
      await this.prisma.$transaction(async (trxPrisma) => {
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { filePath: objectName },
        });
        const newLog = await trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_UPLOAD_SUCCESS,
          },
        });
        this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      });
    } catch (err) {
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_UPLOAD_FAILURE,
          message: err.message,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }
}
