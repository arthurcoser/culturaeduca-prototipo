import { IsEnum } from 'class-validator';
import { DataIngestionFileType } from '@/../prisma/generated/client';

export class CreateDataIngestionDto {
  @IsEnum(DataIngestionFileType)
  type: DataIngestionFileType;
}
