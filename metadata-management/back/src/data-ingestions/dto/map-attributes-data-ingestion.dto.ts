import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Min,
  ValidateNested,
} from 'class-validator';
import { DataIngestionGeoType } from '@/../prisma/generated/client';

class CreateDataIngestionColumnDto {
  @IsInt()
  @Min(1)
  attributeId: number;

  @IsString()
  @IsNotEmpty()
  propertyName: string;
}

class CreateDataIngestionGeoDto {
  @IsInt()
  @Min(1)
  attributeId: number;

  @IsEnum(DataIngestionGeoType)
  type: DataIngestionGeoType;

  @IsOptional()
  @IsString()
  propertyNameLat: string | null;

  @IsOptional()
  @IsString()
  propertyNameLng: string | null;

  @IsOptional()
  @IsString()
  propertyNameWkt: string | null;
}

export class MapAttributesDataIngestionDto {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateDataIngestionColumnDto)
  columns: CreateDataIngestionColumnDto[];

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateDataIngestionGeoDto)
  geo: CreateDataIngestionGeoDto[];
}
