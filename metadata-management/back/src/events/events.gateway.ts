import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({
  cors: 'http://localhost:8081',
})
export class EventsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer() server;

  @SubscribeMessage('message')
  handleMessage(client: any, payload: any): string {
    console.log(new Date(), client.id, payload);
    return 'Hello world!';
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  afterInit(server: Server) {
    console.log('WebSocket Server init');
  }

  handleConnection(client: Socket) {
    console.log('Client connected ', client.id);
  }

  handleDisconnect(client: Socket) {
    console.log('Client disconnect ', client.id);
  }
}
