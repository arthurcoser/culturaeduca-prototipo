import { IsEnum, IsInt, IsString, Min } from 'class-validator';
import { GeoserverLayerCategory } from '@/../prisma/generated/client';

export class CreateGeoserverLayerDto {
  @IsEnum(GeoserverLayerCategory)
  category: GeoserverLayerCategory;

  @IsString()
  name: string;
}
