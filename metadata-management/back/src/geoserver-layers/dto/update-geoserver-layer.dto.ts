import { PartialType } from '@nestjs/mapped-types';
import { CreateGeoserverLayerDto } from './create-geoserver-layer.dto';
export class UpdateGeoserverLayerDto extends PartialType(
  CreateGeoserverLayerDto,
) {}
