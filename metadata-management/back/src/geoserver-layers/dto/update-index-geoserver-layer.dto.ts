import { IsInt, IsOptional, Min } from 'class-validator';

export class UpdateIndexGeoserverLayerDto {
  @IsInt()
  @Min(1)
  index: number;
}
