import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { GeoserverLayersService } from './geoserver-layers.service';
import { UpdateGeoserverLayerDto, UpdateIndexGeoserverLayerDto } from './dto';

@Controller('geoserver_layers')
export class GeoserverLayersController {
  constructor(
    private readonly geoserverLayersService: GeoserverLayersService,
  ) {}

  // Get

  @Get('published')
  findAllPublished() {
    return this.geoserverLayersService.findAllPublished();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.geoserverLayersService.findOne(id);
  }

  @Get()
  findAll() {
    return this.geoserverLayersService.findAll();
  }

  // Patch

  @Patch(':id/index')
  updateIndex(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateIndexGeoserverLayerDto: UpdateIndexGeoserverLayerDto,
  ) {
    return this.geoserverLayersService.updateIndex(
      id,
      updateIndexGeoserverLayerDto,
    );
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateGeoserverLayerDto: UpdateGeoserverLayerDto,
  ) {
    return this.geoserverLayersService.update(id, updateGeoserverLayerDto);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.geoserverLayersService.remove(id);
  }
}
