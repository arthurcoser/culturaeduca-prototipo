import { Module } from '@nestjs/common';
import { GeoserverLayersService } from './geoserver-layers.service';
import { GeoserverLayersController } from './geoserver-layers.controller';
import { PrismaService } from '@/prisma.service';

@Module({
  controllers: [GeoserverLayersController],
  providers: [GeoserverLayersService, PrismaService],
})
export class GeoserverLayersModule {}
