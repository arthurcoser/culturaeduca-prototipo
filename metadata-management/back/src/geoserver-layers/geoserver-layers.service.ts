import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UpdateGeoserverLayerDto, UpdateIndexGeoserverLayerDto } from './dto';
import { PrismaService } from '@/prisma.service';
import { GeoserverLayer } from '@/../prisma/generated/client';

@Injectable()
export class GeoserverLayersService {
  constructor(private readonly prisma: PrismaService) {}

  findAll(): Promise<GeoserverLayer[]> {
    return this.prisma.geoserverLayer.findMany({
      include: {
        catalog: {
          include: {
            versions: true,
          },
        },
      },
      orderBy: [{ category: 'asc' }, { index: 'asc' }],
    });
  }

  findAllPublished(): Promise<any[]> {
    return this.prisma.geoserverLayer.findMany({
      where: {
        catalog: {
          versions: {
            some: {
              published: true,
            },
          },
        },
      },
      select: {
        id: true,
        category: true,
        name: true,
        index: true,
        catalog: {
          select: {
            id: true,
            name: true,
            description: true,
            type: true,
            versions: {
              where: {
                published: true,
              },
              select: {
                id: true,
                name: true,
                description: true,
                tableName: true,
                published: true,
                default: true,
              },
            },
          },
        },
      },
      orderBy: [{ category: 'asc' }, { index: 'asc' }],
    });
  }

  async findOne(id: number) {
    const geoserverLayer = await this.prisma.geoserverLayer.findUnique({
      where: { id },
    });
    if (!geoserverLayer) {
      throw new NotFoundException('Geoserver Layer not found!');
    }
    return geoserverLayer;
  }

  async update(
    id: number,
    dto: UpdateGeoserverLayerDto,
  ): Promise<GeoserverLayer> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find geoserver layer
      let geoserverLayer = await trxPrisma.geoserverLayer.findUnique({
        where: { id },
      });
      if (!geoserverLayer) {
        throw new NotFoundException('Geoserver Layer not found!');
      }
      const oldCategory = geoserverLayer.category;
      const newCategory = dto?.category;
      let newIndex = geoserverLayer.index;
      // if new category is different, update indexes
      if (newCategory !== oldCategory) {
        // update indexes of the layer from the old category
        await trxPrisma.geoserverLayer.updateMany({
          where: {
            category: oldCategory,
            index: { gt: geoserverLayer.index },
          },
          data: {
            index: {
              decrement: 1,
            },
          },
        });
        // define new index
        const layerNewCategoryMaxIndex =
          await this.prisma.geoserverLayer.groupBy({
            by: ['category'],
            where: {
              category: newCategory,
            },
            _max: {
              index: true,
            },
          });
        newIndex = (layerNewCategoryMaxIndex?.[0]?._max?.index || 0) + 1;
      }
      // update geoserver layer
      geoserverLayer = await trxPrisma.geoserverLayer.update({
        where: {
          id,
        },
        data: {
          ...(newCategory ? { category: newCategory } : {}),
          ...(dto?.name !== undefined
            ? { name: dto.name?.trim() || null }
            : {}),
          index: newIndex,
        },
      });
      return geoserverLayer;
    });
  }

  async updateIndex(
    id: number,
    dto: UpdateIndexGeoserverLayerDto,
  ): Promise<GeoserverLayer> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find geoserver layer
      let geoserverLayer = await trxPrisma.geoserverLayer.findUnique({
        where: { id },
      });
      if (!geoserverLayer) {
        throw new NotFoundException('Geoserver Layer not found!');
      }
      const oldIndex = geoserverLayer.index;
      // get new index (if greater than max index, set it to max index)
      let newIndex = dto.index;
      const layerSameCategoryMaxIndex = await trxPrisma.geoserverLayer.groupBy({
        by: ['category'],
        where: {
          category: geoserverLayer.category,
        },
        _max: {
          index: true,
        },
      });
      const maxIndex = layerSameCategoryMaxIndex?.[0]?._max?.index || 0;
      newIndex = newIndex > maxIndex ? maxIndex : newIndex;
      if (newIndex <= 0) {
        throw new BadRequestException(
          'New index must be more than or equal to 1!',
        );
      }
      // if same index, do nothing
      if (newIndex === oldIndex) {
        return geoserverLayer;
      }
      // if new index less than old index
      if (newIndex < oldIndex) {
        await trxPrisma.geoserverLayer.updateMany({
          where: {
            index: { gte: newIndex, lt: oldIndex },
          },
          data: { index: { increment: 1 } },
        });
      }
      // if new index greater than old index
      else if (newIndex > oldIndex) {
        await trxPrisma.geoserverLayer.updateMany({
          where: {
            index: { gt: oldIndex, lte: newIndex },
          },
          data: { index: { decrement: 1 } },
        });
      }
      geoserverLayer = await trxPrisma.geoserverLayer.update({
        where: { id: geoserverLayer.id },
        data: { index: newIndex },
      });
      return geoserverLayer;
    });
  }

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // delete geoserver layer
      const geoserverLayer = await trxPrisma.geoserverLayer.delete({
        where: {
          id,
        },
      });
      if (!geoserverLayer) {
        throw new NotFoundException('Geoserver Layer not found!');
      }
      // update indexes of the layer from the same category
      await trxPrisma.geoserverLayer.updateMany({
        where: {
          category: geoserverLayer.category,
          index: { gt: geoserverLayer.index },
        },
        data: {
          index: {
            decrement: 1,
          },
        },
      });
      return geoserverLayer;
    });
  }
}
