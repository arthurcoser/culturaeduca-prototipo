import { ConfigService } from '@nestjs/config';
import { Provider } from '@nestjs/common';
import knex, { Knex } from 'knex';

export const KnexConnectionBases = 'KnexConnectionBases';

export const KnexProviderBases: Provider = {
  provide: KnexConnectionBases,
  useFactory: async (configService: ConfigService): Promise<Knex> =>
    knex(configService.get<Knex.Config>('knex.bases')),
  inject: [ConfigService],
};
