import {
  DynamicModule,
  MiddlewareConsumer,
  Module,
  NestModule,
  Provider,
} from '@nestjs/common';
import {
  ConfigInjectionToken,
  MinioModuleAsyncConfig,
  MinioModuleConfig,
} from './config.interface';
import { MinioService } from './minio.service';
import { MinioController } from './minio.controller';

@Module({})
export class MinioModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply().forRoutes('*');
  }

  static forRoot({
    endPoint,
    port,
    useSSL,
    accessKey,
    secretKey,
  }: MinioModuleConfig): DynamicModule {
    return {
      providers: [
        {
          useValue: {
            endPoint,
            port,
            useSSL,
            accessKey,
            secretKey,
          },
          provide: ConfigInjectionToken,
        },
        MinioService,
      ],
      controllers: [MinioController],
      exports: [MinioService],
      imports: [],
      module: MinioModule,
    };
  }

  static forRootAsync(options: MinioModuleAsyncConfig): DynamicModule {
    const asyncProviders = this.createAsyncProviders(options);
    return {
      providers: asyncProviders,
      controllers: [MinioController],
      exports: [MinioService],
      imports: [],
      module: MinioModule,
    };
  }

  private static createAsyncProviders(
    options: MinioModuleAsyncConfig,
  ): Provider[] {
    return [
      {
        provide: ConfigInjectionToken,
        useFactory: options.useFactory,
        inject: options.inject || [],
      },
      MinioService,
    ];
  }
}
