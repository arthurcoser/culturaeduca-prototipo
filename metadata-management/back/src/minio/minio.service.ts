import { Inject, Injectable } from '@nestjs/common';
import * as Minio from 'minio';
import * as fs from 'fs/promises';
import { ConfigInjectionToken, MinioModuleConfig } from './config.interface';

@Injectable()
export class MinioService {
  private minioClient: Minio.Client;
  private bucketName = 'test-bucket';

  constructor(@Inject(ConfigInjectionToken) private config: MinioModuleConfig) {
    this.minioClient = new Minio.Client({
      endPoint: config.endPoint,
      port: config.port,
      useSSL: config.useSSL,
      accessKey: config.accessKey,
      secretKey: config.secretKey,
    });
  }

  async listBuckets() {
    return this.minioClient.listBuckets();
  }

  async fPutObject(objectName: string, file: Express.Multer.File) {
    const metadata = {
      originalname: file.originalname,
    };
    const object = await this.minioClient.fPutObject(
      this.bucketName,
      objectName,
      file.path,
      metadata,
    );
    fs.unlink(file.path);
    return object;
  }

  async fGetObject(objectName: string) {
    const tmpPath = `static/tmp/${objectName}`;
    await this.minioClient.fGetObject(this.bucketName, objectName, tmpPath);
    return tmpPath;
  }

  async statObject(objectName: string) {
    return this.minioClient.statObject(this.bucketName, objectName);
  }
}
