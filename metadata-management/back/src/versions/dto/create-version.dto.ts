import {
  IsDateString,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateVersionDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsOptional()
  @IsDateString()
  initialDate: string | null;

  @IsOptional()
  @IsDateString()
  endDate: string | null;
}
