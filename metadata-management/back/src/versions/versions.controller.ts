import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { VersionsService } from './versions.service';
import { GetTableBasesVersionQueryDto, UpdateVersionDto } from './dto';
import { CreateAttributeDto } from '@/attributes/dto';
import { CreateDataIngestionDto } from '@/data-ingestions/dto';

@Controller('versions')
export class VersionsController {
  constructor(private readonly versionsService: VersionsService) {}

  // Get

  @Get(':id/attributes')
  findAttributes(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findAttributes(id);
  }

  @Get(':id/data_ingestions')
  findDataIngestions(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findDataIngestions(id);
  }

  @Get(':id/table_bases')
  getTableBases(
    @Param('id', ParseIntPipe) id: number,
    @Query() getTableBasesVersionQueryDto: GetTableBasesVersionQueryDto,
  ) {
    return this.versionsService.getTableBases(id, getTableBasesVersionQueryDto);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findOne(id);
  }

  // Post

  @Post(':id/publish')
  publish(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.publish(id);
  }

  @Post(':id/unpublish')
  unpublish(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.unpublish(id);
  }

  @Post(':id/set_default')
  setDefault(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.setDefault(id);
  }

  @Post(':id/attributes_bulk')
  bulkCreateAttribute(
    @Param('id', ParseIntPipe) id: number,
    @Body() createAttributeDtos: CreateAttributeDto[],
  ) {
    return this.versionsService.bulkCreateAttributes(id, createAttributeDtos);
  }

  @Post(':id/attributes')
  createAttribute(
    @Param('id', ParseIntPipe) id: number,
    @Body() createAttributeDto: CreateAttributeDto,
  ) {
    return this.versionsService.createAttribute(id, createAttributeDto);
  }

  @Post(':id/data_ingestion')
  createDataIngestion(
    @Param('id', ParseIntPipe) id: number,
    @Body() createDataIngestionDto: CreateDataIngestionDto,
  ) {
    return this.versionsService.createDataIngestion(id, createDataIngestionDto);
  }

  // Patch

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateVersionDto: UpdateVersionDto,
  ) {
    return this.versionsService.update(id, updateVersionDto);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.remove(id);
  }
}
