import { ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';

import { KnexProviderBases } from '@/knex.provider';
import { PrismaService } from '@/prisma.service';
import { VersionsController } from './versions.controller';
import { VersionsService } from './versions.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        ...configService.get<MongooseModuleFactoryOptions>('mongoose'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [VersionsController],
  providers: [VersionsService, PrismaService, KnexProviderBases],
})
export class VersionsModule {}
