import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Knex } from 'knex';

import {
  Attribute,
  DataIngestion,
  DataIngestionLogStatus,
  Version,
} from '@/../prisma/generated/client';
import { CreateAttributeDto } from '@/attributes/dto';
import { CreateDataIngestionDto } from '@/data-ingestions/dto';
import { KnexConnectionBases } from '@/knex.provider';
import { PrismaService } from '@/prisma.service';
import {
  TypeAttributeEnum,
  typesAttributesKnexColumnFunction,
} from '@/attributes/attributes.constants';
import { GetTableBasesVersionQueryDto, UpdateVersionDto } from './dto';
import * as mongoose from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';

@Injectable()
export class VersionsService {
  constructor(
    @InjectConnection() private mongooseConnection: mongoose.Connection,
    @Inject(KnexConnectionBases) private readonly knexBases: Knex,
    private readonly prisma: PrismaService,
  ) {}

  async bulkCreateAttributes(
    id: number,
    dtos: CreateAttributeDto[],
  ): Promise<Attribute[]> {
    // find version
    const version = await this.prisma.version.findUnique({
      where: { id },
      include: {
        attributes: {
          orderBy: { order: 'desc' },
        },
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    const newOrder = (version.attributes?.[0]?.order || -1) + 1;
    return this.prisma.$transaction(async (trxPrisma) => {
      const dataAttributes = dtos.map((dto, index) => ({
        versionId: version.id,
        name: dto?.name?.trim() || null,
        description: dto?.description?.trim() || null,
        type: dto?.type?.trim() || null,
        optional: dto?.optional !== undefined ? dto.optional : null,
        indexable: dto?.indexable !== undefined ? dto.indexable : null,
        unique: dto?.unique !== undefined ? dto.unique : null,
        sensitiveData:
          dto?.sensitiveData !== undefined ? dto.sensitiveData : null,
        order: newOrder + index,
        defaultValue: dto?.defaultValue?.trim() || null,
        unit: dto?.unit?.trim() || null,
        decodeValue: dto?.decodeValue?.trim() || null,
      }));
      const attributes = await Promise.all(
        dataAttributes.map(async (dataAttribute) => {
          const attribute = await trxPrisma.attribute.create({
            data: dataAttribute,
          });

          return attribute;
        }),
      );
      await this.knexBases.transaction(async (trxKnex) => {
        await Promise.all(
          attributes.map((attribute) =>
            trxKnex.schema.alterTable(version.tableName, async (table) => {
              const columnName = attribute.name;
              // create column
              const knexColumnFunction =
                typesAttributesKnexColumnFunction?.[attribute.type] || 'text';
              const knexNullableFunction = attribute.optional
                ? 'nullable'
                : 'notNullable';
              table[knexColumnFunction](columnName)[knexNullableFunction]();
              // unique
              if (attribute.unique) {
                table.unique([columnName]);
              }
              // index
              if (attribute.indexable) {
                table.index(columnName);
              }
            }),
          ),
        );
      });
      return attributes;
    });
  }

  async createAttribute(
    id: number,
    dto: CreateAttributeDto,
  ): Promise<Attribute> {
    const version = await this.prisma.version.findUnique({
      where: { id },
      include: {
        attributes: {
          orderBy: { order: 'desc' },
        },
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    // create attribute and column in bases table
    const newOrder = (version.attributes?.[0]?.order ?? -1) + 1;
    return this.prisma.$transaction(async (trxPrisma) => {
      // create attribute
      const attribute = await trxPrisma.attribute.create({
        data: {
          versionId: version.id,
          name: dto?.name?.trim() || null,
          description: dto?.description?.trim() || null,
          type: dto?.type?.trim() || null,
          optional: dto?.optional !== undefined ? dto.optional : null,
          indexable: dto?.indexable !== undefined ? dto.indexable : null,
          unique: dto?.unique !== undefined ? dto.unique : null,
          sensitiveData:
            dto?.sensitiveData !== undefined ? dto.sensitiveData : null,
          order: newOrder,
          defaultValue: dto?.defaultValue?.trim() || null,
          unit: dto?.unit?.trim() || null,
          decodeValue: dto?.decodeValue?.trim() || null,
        },
      });
      // create column in bases table
      await this.knexBases.transaction(async (trxKnex) => {
        await trxKnex.schema.alterTable(version.tableName, (table) => {
          const columnName = attribute.name;
          // create column
          const knexColumnFunction =
            typesAttributesKnexColumnFunction?.[attribute.type] || 'text';
          const knexNullableFunction = attribute.optional
            ? 'nullable'
            : 'notNullable';
          table[knexColumnFunction](columnName)[knexNullableFunction]();
          // unique
          if (attribute.unique) {
            table.unique([columnName]);
          }
          // index
          if (attribute.indexable) {
            table.index(columnName);
          }
        });
      });
      return attribute;
    });
  }

  async createDataIngestion(
    id: number,
    dto: CreateDataIngestionDto,
  ): Promise<DataIngestion> {
    const version = await this.prisma.version.findUnique({
      where: { id },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return this.prisma.$transaction(async (trxPrisma) => {
      const dataIngestion = await trxPrisma.dataIngestion.create({
        data: {
          versionId: version.id,
          filePath: null,
          fileType: dto?.type,
          collectionName: null,
          collectionSchema: undefined,
          logs: {
            create: {
              status: DataIngestionLogStatus.CREATED,
            },
          },
        },
      });
      const collectionName = `data_ingestion_${dataIngestion.id}`;
      const collections = await this.mongooseConnection.db
        .listCollections({ name: collectionName })
        .toArray();
      if (collections.length > 0) {
        await this.mongooseConnection.dropCollection(collectionName);
      }
      await this.mongooseConnection.createCollection(collectionName);
      return trxPrisma.dataIngestion.update({
        where: { id: dataIngestion.id },
        data: { collectionName },
      });
    });
  }

  async findAttributes(id: number): Promise<Attribute[]> {
    const version = await this.prisma.version.findUnique({
      where: { id },
      include: {
        attributes: {
          orderBy: {
            order: 'asc',
          },
        },
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return version.attributes;
  }

  async findDataIngestions(id: number): Promise<DataIngestion[]> {
    const version = await this.prisma.version.findUnique({
      where: { id },
      include: {
        dataIngestions: {
          orderBy: {
            id: 'asc',
          },
        },
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return version.dataIngestions;
  }

  async findOne(id: number): Promise<Version> {
    const version = await this.prisma.version.findUnique({
      where: { id },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return version;
  }

  async getTableBases(
    id: number,
    queryDto: GetTableBasesVersionQueryDto,
  ): Promise<{ total: number; results: any[] }> {
    // find version
    const version = await this.prisma.version.findUnique({
      where: { id },
      include: {
        attributes: {
          orderBy: { order: 'asc' },
        },
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    // query data
    const perPage = queryDto?.perPage || 10;
    const page = queryDto?.page || 1;
    const limit = perPage;
    const offset = perPage * (page - 1);
    // attributes query
    const defaultColumns = [
      '_id',
      '_data_ingestion_id',
      '_created_at',
      '_updated_at',
    ];
    const attributesNamesQuery = version.attributes
      .filter(
        (attribute) =>
          !attribute.sensitiveData &&
          attribute.type !== TypeAttributeEnum.GEOMETRY,
      )
      .map((attribute) => attribute.name);
    const attributesGeoRawQuery = version.attributes
      .filter(
        (attribute) =>
          !attribute.sensitiveData &&
          attribute.type === TypeAttributeEnum.GEOMETRY,
      )
      .map((attribute) =>
        this.knexBases.raw(
          `ST_GeometryType("${attribute.name}") as "${attribute.name}"`,
        ),
      );
    // perform query
    const results = await this.knexBases
      .table(version.tableName)
      .select([
        ...defaultColumns,
        ...attributesNamesQuery,
        ...attributesGeoRawQuery,
      ])
      .limit(limit)
      .offset(offset);
    // count total
    const count = await this.knexBases.table(version.tableName).count();
    // return results
    return { total: count?.[0]?.count, results };
  }

  async publish(id: number): Promise<Version> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // Find version
      let version = await trxPrisma.version.findUnique({
        where: { id },
      });
      // Valid arguments
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      if (version.published) {
        throw new BadRequestException('Version is already published!');
      }
      if (!version.tableRowsCount) {
        throw new BadRequestException('Version does not have data to publish!');
      }
      // remember to make it default if it's the only one published
      const anyDefaultVersion = await trxPrisma.version.findFirst({
        where: { catalogId: version.catalogId, default: true },
      });
      // update version
      version = await trxPrisma.version.update({
        where: { id: version.id },
        data: {
          published: true,
          default: !anyDefaultVersion,
        },
      });
      return version;
    });
  }

  async remove(id: number): Promise<Version> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // remove version
      const version = await trxPrisma.version.delete({
        where: { id },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      // drop table
      await this.knexBases.transaction(async (trxKnex) => {
        const tableName = version.tableName;
        if (await trxKnex.schema.hasTable(tableName)) {
          await trxKnex.schema.dropTable(tableName);
        }
      });
      return version;
    });
  }

  async setDefault(id: number): Promise<Version> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find version
      let version = await trxPrisma.version.findUnique({
        where: { id },
      });
      // valid arguments
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      if (!version.published) {
        throw new NotFoundException('Version is not published!');
      }
      if (version.default) {
        throw new NotFoundException('Version is already default!');
      }
      // update version
      version = await trxPrisma.version.update({
        where: { id: version.id },
        data: { default: true },
      });
      // update other versions
      await trxPrisma.version.updateMany({
        where: { id: { not: version.id }, catalogId: version.catalogId },
        data: { default: false },
      });
      return version;
    });
  }

  async unpublish(id: number): Promise<Version> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // Find version
      let version = await trxPrisma.version.findUnique({
        where: { id },
      });
      // Valid arguments
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      if (!version.published) {
        throw new BadRequestException('Version is not published!');
      }
      // if version is default, make another one the default
      if (version.default) {
        const newDefaultVersion = await trxPrisma.version.findFirst({
          where: {
            id: { not: version.id },
            catalogId: version.catalogId,
            published: true,
          },
          orderBy: { id: 'desc' },
        });
        if (newDefaultVersion) {
          await trxPrisma.version.update({
            where: { id: newDefaultVersion.id },
            data: { default: true },
          });
        }
      }
      // update version
      version = await trxPrisma.version.update({
        where: { id: version.id },
        data: {
          published: false,
          default: false,
        },
      });
      return version;
    });
  }

  async update(id: number, dto: UpdateVersionDto): Promise<Version> {
    const version = await this.prisma.version.update({
      where: { id },
      data: {
        ...(dto.name !== undefined ? { name: dto.name?.trim() || null } : {}),
        ...(dto.description !== undefined
          ? { description: dto.description?.trim() || null }
          : {}),
        ...(dto.initialDate !== undefined
          ? { initialDate: dto.initialDate?.trim() || null }
          : {}),
        ...(dto.endDate !== undefined
          ? { endDate: dto.endDate?.trim() || null }
          : {}),
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return version;
  }
}
