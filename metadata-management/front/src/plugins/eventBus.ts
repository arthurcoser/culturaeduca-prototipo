import mitt from "mitt";

export const EventBus = mitt();

export const SnackbarAlertEventKey = "snackbar-alert";

export interface SnackbarAlertEventOptions {
  message?: string;
  color?: string;
  timeout?: number;
  error?: any;
}

export function emitAlert(options: SnackbarAlertEventOptions) {
  EventBus.emit(SnackbarAlertEventKey, options);
}

export function emitAlertError(error: any) {
  emitAlert({ color: "error", timeout: 5000, error });
}

export function emitAlertSuccess(message: string) {
  emitAlert({ message, color: "success", timeout: 3000 });
}
