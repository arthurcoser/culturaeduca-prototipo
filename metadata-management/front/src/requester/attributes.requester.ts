import axios from "axios";
import { GeoJSON } from "leaflet";

import { Attribute } from "@backPrisma/generated/client";
import { UpdateAttributeDto } from "@backSrc/attributes/dto";

const findOne = async (attributeId: number): Promise<Attribute> => {
  const res = await axios.get<Attribute>(`attributes/${attributeId}`);
  return res.data;
};

const getBasesGeojson = async (
  attributeId: number,
  basesDataId: number
): Promise<GeoJSON.Feature> => {
  const res = await axios.get<GeoJSON.Feature>(
    `attributes/${attributeId}/bases_geojson/${basesDataId}`
  );
  return res.data;
};

const remove = async (attributeId: number): Promise<Attribute> => {
  const res = await axios.delete<Attribute>(`attributes/${attributeId}`);
  return res.data;
};

const update = async (
  attributeId: number,
  dto: UpdateAttributeDto
): Promise<Attribute> => {
  const res = await axios.patch<Attribute>(`attributes/${attributeId}`, dto);
  return res.data;
};

export default { findOne, getBasesGeojson, remove, update };
