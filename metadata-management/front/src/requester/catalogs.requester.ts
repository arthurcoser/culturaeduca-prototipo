import axios from "axios";

import { Catalog, GeoserverLayer, Version } from "@backPrisma/generated/client";
import { CreateCatalogDto, UpdateCatalogDto } from "@backSrc/catalogs/dto";
import { CreateVersionDto } from "@backSrc/versions/dto";
import { CreateGeoserverLayerDto } from "@backSrc/geoserver-layers/dto";

const create = async (dto: CreateCatalogDto): Promise<Catalog> => {
  const res = await axios.post<Catalog>("catalogs", dto);
  return res.data;
};

const createGeoserverLayer = async (
  catalogId: number,
  dto: CreateGeoserverLayerDto
): Promise<GeoserverLayer> => {
  const res = await axios.post<GeoserverLayer>(
    `catalogs/${catalogId}/geoserver_layers`,
    dto
  );
  return res.data;
};

const createVersion = async (
  catalogId: number,
  dto: CreateVersionDto
): Promise<Version> => {
  const res = await axios.post<Version>(`catalogs/${catalogId}/versions`, dto);
  return res.data;
};

const findAll = async (): Promise<Catalog[]> => {
  const res = await axios.get<Catalog[]>("catalogs");
  return res.data;
};

const findOne = async (catalogId: number): Promise<Catalog> => {
  const res = await axios.get<Catalog>(`catalogs/${catalogId}`);
  return res.data;
};

const findVersions = async (catalogId: number): Promise<Version[]> => {
  const res = await axios.get<Version[]>(`catalogs/${catalogId}/versions`);
  return res.data;
};

const remove = async (versionId: number): Promise<Catalog> => {
  const res = await axios.delete<Catalog>(`catalogs/${versionId}`);
  return res.data;
};

const update = async (
  catalogId: number,
  dto: UpdateCatalogDto
): Promise<Catalog> => {
  const res = await axios.patch<Catalog>(`catalogs/${catalogId}`, dto);
  return res.data;
};

export default {
  create,
  createGeoserverLayer,
  createVersion,
  findAll,
  findOne,
  findVersions,
  remove,
  update,
};
