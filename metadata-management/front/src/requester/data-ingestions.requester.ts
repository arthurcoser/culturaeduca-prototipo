import axios from "axios";

import { DataIngestion, DataIngestionLog } from "@backPrisma/generated/client";
import { GetCollectionDataIngestionQueryDto } from "@backSrc/data-ingestions/dto";

const findCurrentLog = async (
  dataIngestionId: number
): Promise<DataIngestionLog> => {
  const res = await axios.get<DataIngestionLog>(
    `data_ingestions/${dataIngestionId}/logs/current`
  );
  return res.data;
};

const findLogs = async (
  dataIngestionId: number
): Promise<DataIngestionLog[]> => {
  const res = await axios.get<DataIngestionLog[]>(
    `data_ingestions/${dataIngestionId}/logs`
  );
  return res.data;
};

const findOne = async (dataIngestionId: number): Promise<DataIngestion> => {
  const res = await axios.get<DataIngestion>(
    `data_ingestions/${dataIngestionId}`
  );
  return res.data;
};

const getCollection = async (
  dataIngestionId: number,
  queryObject: GetCollectionDataIngestionQueryDto
): Promise<{ total: number; results: any[] }> => {
  const res = await axios.get<{
    total: number;
    results: any[];
  }>(`data_ingestions/${dataIngestionId}/collection`, {
    params: queryObject,
  });
  return res.data;
};

const getFileInfo = async (dataIngestionId: number): Promise<any> => {
  const res = await axios.get<any>(
    `data_ingestions/${dataIngestionId}/file_info`
  );
  return res.data;
};

const loadData = async (dataIngestionId: number): Promise<void> => {
  await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/load_data`
  );
};

const mapAttributes = async (
  dataIngestionId: number,
  dto: any
): Promise<void> => {
  await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/map_attributes`,
    dto
  );
};

const parseFile = async (dataIngestionId: number): Promise<void> => {
  await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/parse_file`
  );
};

const remove = async (dataIngestionId: number): Promise<DataIngestion> => {
  const res = await axios.delete<DataIngestion>(
    `data_ingestions/${dataIngestionId}`
  );
  return res.data;
};

const removeCurrentLog = async (
  dataIngestionId: number
): Promise<DataIngestionLog> => {
  const res = await axios.delete<DataIngestionLog>(
    `data_ingestions/${dataIngestionId}/logs/current`
  );
  return res.data;
};

const uploadFile = async (
  dataIngestionId: number,
  formData: FormData
): Promise<DataIngestion> => {
  const res = await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/upload_file`,
    formData
  );
  return res.data;
};

export default {
  findCurrentLog,
  findLogs,
  findOne,
  getCollection,
  getFileInfo,
  loadData,
  mapAttributes,
  parseFile,
  remove,
  removeCurrentLog,
  uploadFile,
};
