import axios from "axios";

import { Catalog, GeoserverLayer, Version } from "@backPrisma/generated/client";
import {
  UpdateGeoserverLayerDto,
  UpdateIndexGeoserverLayerDto,
} from "@backSrc/geoserver-layers/dto";

const findAll = async () => {
  const res = await axios.get<
    Array<GeoserverLayer & { catalog: Catalog & { versions: Version } }>
  >("geoserver_layers");
  return res.data;
};

const remove = async (geoserverLayerId: number) => {
  const res = await axios.delete<GeoserverLayer>(
    `geoserver_layers/${geoserverLayerId}`
  );
  return res.data;
};

const update = async (
  geoserverLayerId: number,
  dto: UpdateGeoserverLayerDto
) => {
  const res = await axios.patch<GeoserverLayer>(
    `geoserver_layers/${geoserverLayerId}`,
    dto
  );
  return res.data;
};

const updateIndex = async (
  geoserverLayerId: number,
  dto: UpdateIndexGeoserverLayerDto
) => {
  const res = await axios.patch<GeoserverLayer>(
    `geoserver_layers/${geoserverLayerId}/index`,
    dto
  );
  return res.data;
};

export default {
  findAll,
  remove,
  update,
  updateIndex,
};
