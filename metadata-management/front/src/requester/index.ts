import axios from "axios";

import attributes from "./attributes.requester";
import catalogs from "./catalogs.requester";
import dataIngestions from "./data-ingestions.requester";
import geoserverLayers from "./geoserver-layers.requester";
import versions from "./versions.requester";

axios.defaults.baseURL = process.env.VUE_APP_URL_API || "http://localhost:3000";
axios.defaults.withCredentials = true;

export default {
  attributes,
  catalogs,
  dataIngestions,
  geoserverLayers,
  versions,
};
