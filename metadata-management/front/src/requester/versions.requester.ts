import axios from "axios";

import {
  Attribute,
  DataIngestion,
  Version,
} from "@backPrisma/generated/client";
import { CreateAttributeDto } from "@backSrc/attributes/dto";
import { CreateDataIngestionDto } from "@backSrc/data-ingestions/dto";
import {
  GetTableBasesVersionQueryDto,
  UpdateVersionDto,
} from "@backSrc/versions/dto";

const createAttribute = async (
  versionId: number,
  dto: CreateAttributeDto
): Promise<Attribute> => {
  const res = await axios.post<Attribute>(
    `versions/${versionId}/attributes`,
    dto
  );
  return res.data;
};

const createAttributeBulk = async (
  versionId: number,
  dto: CreateAttributeDto[]
): Promise<Attribute> => {
  const res = await axios.post<Attribute>(
    `versions/${versionId}/attributes_bulk`,
    dto
  );
  return res.data;
};

const createDataIngestion = async (
  versionId: number,
  dto: CreateDataIngestionDto
): Promise<DataIngestion> => {
  const res = await axios.post<DataIngestion>(
    `versions/${versionId}/data_ingestion`,
    dto
  );
  return res.data;
};

const findAttributes = async (versionId: number): Promise<Attribute[]> => {
  const res = await axios.get<Attribute[]>(`versions/${versionId}/attributes`);
  return res.data;
};

const findDataIngestions = async (
  versionId: number
): Promise<DataIngestion[]> => {
  const res = await axios.get<DataIngestion[]>(
    `versions/${versionId}/data_ingestions`
  );
  return res.data;
};

const findOne = async (versionId: number): Promise<Version> => {
  const res = await axios.get<Version>(`versions/${versionId}`);
  return res.data;
};

const getTableBases = async (
  versionId: number,
  queryObject: GetTableBasesVersionQueryDto
): Promise<{ total: number; results: any[] }> => {
  const res = await axios.get<{
    total: number;
    results: any[];
  }>(`versions/${versionId}/table_bases`, {
    params: queryObject,
  });
  return res.data;
};

const publish = async (versionId: number): Promise<Version> => {
  const res = await axios.post<Version>(`versions/${versionId}/publish`);
  return res.data;
};

const remove = async (versionId: number): Promise<Version> => {
  const res = await axios.delete<Version>(`versions/${versionId}`);
  return res.data;
};

const setDefault = async (versionId: number): Promise<Version> => {
  const res = await axios.post<Version>(`versions/${versionId}/set_default`);
  return res.data;
};

const unpublish = async (versionId: number): Promise<Version> => {
  const res = await axios.post<Version>(`versions/${versionId}/unpublish`);
  return res.data;
};

const update = async (
  versionId: number,
  dto: UpdateVersionDto
): Promise<Version> => {
  const res = await axios.patch<Version>(`versions/${versionId}`, dto);
  return res.data;
};

export default {
  createAttribute,
  createAttributeBulk,
  createDataIngestion,
  findAttributes,
  findDataIngestions,
  findOne,
  getTableBases,
  publish,
  remove,
  setDefault,
  unpublish,
  update,
};
