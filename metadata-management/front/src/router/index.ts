// Composables
import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";

const routes: RouteRecordRaw[] = [
  /**
   * DEFAULT ROUTES
   */
  {
    path: "/",
    component: () =>
      import(/* webpackChunkName: "home" */ "@/layouts/LayoutDefault.vue"),
    /**
     * LAYOUT DEFAULT
     */
    children: [
      {
        path: "",
        name: "Home",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Home.vue"),
      },
      {
        path: "catalogs",
        name: "CatalogsIndex",
        component: () =>
          import(
            /* webpackChunkName: "catalogs" */ "@/pages/catalogs/CatalogsIndex.vue"
          ),
      },
      {
        path: "geoserver_layers",
        name: "GeoserverLayersIndex",
        component: () =>
          import(
            /* webpackChunkName: "geoserver-layers" */ "@/pages/geoserver-layers/GeoserverLayersIndex.vue"
          ),
      },
      {
        path: "catalogs/:catalogId",
        component: () =>
          import(
            /* webpackChunkName: "catalogs" */ "@/layouts/LayoutCatalog.vue"
          ),
        /**
         * LAYOUT CATALOG
         */
        children: [
          {
            path: "",
            component: () =>
              import(
                /* webpackChunkName: "catalogs" */ "@/layouts/LayoutCatalogView.vue"
              ),
            /**
             * LAYOUT CATALOG VIEW
             */
            children: [
              {
                path: "",
                name: "CatalogsView",
                component: () =>
                  import(
                    /* webpackChunkName: "catalogs" */ "@/pages/catalogs/CatalogsView.vue"
                  ),
              },
              {
                path: "versions",
                name: "CatalogsViewVersions",
                component: () =>
                  import(
                    /* webpackChunkName: "catalogs" */ "@/pages/catalogs/CatalogsViewVersions.vue"
                  ),
              },
            ],
          },
          {
            path: "versions/:versionId",
            component: () =>
              import(
                /* webpackChunkName: "versions" */ "@/layouts/LayoutVersionView.vue"
              ),
            /**
             * LAYOUT VERSION VIEW
             */
            children: [
              {
                path: "",
                name: "VersionsView",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsView.vue"
                  ),
              },
              {
                path: "attributes",
                name: "VersionsViewAttributes",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewAttributes.vue"
                  ),
              },
              {
                path: "data_ingestions",
                name: "VersionsViewDataIngestions",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewDataIngestions.vue"
                  ),
              },
              {
                path: "data",
                name: "VersionsViewData",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewData.vue"
                  ),
              },
            ],
          },
          {
            path: "versions/:versionId/import_attributes",
            name: "VersionsImportAttributes",
            component: () =>
              import(
                /* webpackChunkName: "versions" */ "@/pages/versions/VersionsImportAttributes.vue"
              ),
          },
          {
            path: "versions/:versionId/data_ingestions/:dataIngestionId",
            component: () =>
              import(
                /* webpackChunkName: "versions" */ "@/layouts/LayoutDataIngestionView.vue"
              ),
            /**
             * LAYOUT DATA INGESTION VIEW
             */
            children: [
              {
                path: "",
                name: "DataIngestionsView",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsView.vue"
                  ),
              },
              {
                path: "file_upload",
                name: "DataIngestionsViewFileUpload",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewFileUpload.vue"
                  ),
              },
              {
                path: "file_parsing",
                name: "DataIngestionsViewFileParsing",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewFileParsing.vue"
                  ),
              },
              {
                path: "attributes_mapping",
                name: "DataIngestionsViewAttributesMapping",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewAttributesMapping.vue"
                  ),
              },
              {
                path: "data_loading",
                name: "DataIngestionsViewDataLoading",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewDataLoading.vue"
                  ),
              },
            ],
          },
        ],
      },
    ],
  },
  /**
   * MAP PREVIEWS
   */
  {
    path: "/map",
    component: () => import("@/layouts/LayoutMap.vue"),
    children: [
      {
        path: "attributes/:attributeId/preview_geojson/:basesDataId",
        name: "AttributesPreviewGeoJson",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/attributes/AttributesPreviewGeoJson.vue"
          ),
      },
      {
        path: "versions/:versionId/preview_geoserver_layer",
        name: "VersionsPreviewGeoserverLayer",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/versions/VersionsPreviewGeoserverLayer.vue"
          ),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
