const catalogTypeHelper = {
  GEOSPATIAL_POINT: {
    text: "Geospatial Point",
    icon: "mdi-map-marker",
  },
  GEOSPATIAL_LINE: {
    text: "Geospatial Line",
    icon: "mdi-vector-line",
  },
  GEOSPATIAL_POLYGON: {
    text: "Geospatial Polygon",
    icon: "mdi-vector-polygon",
  },
  NON_GEOSPATIAL: {
    text: "Non Geospatial",
    icon: "mdi-table-large",
  },
};

export function getCatalogTypeIcon(catalogType: string) {
  return catalogTypeHelper?.[catalogType]?.icon;
}

export function getCatalogTypeText(catalogType: string) {
  return catalogTypeHelper?.[catalogType]?.text;
}
