const dataIngestionFileTypeHelper = {
  CSV: {
    text: "CSV",
    icon: "mdi-file-delimited",
  },
  GEOJSON: {
    text: "GeoJSON",
    icon: "mdi-file-marker",
  },
  JSON: {
    text: "JSON",
    icon: "mdi-file-code",
  },
  SHAPEFILE: {
    text: "Shapefile",
    icon: "mdi-folder-zip",
  },
};

export function getDataIngestionFileTypeIcon(dataIngestionFileType: string) {
  return dataIngestionFileTypeHelper?.[dataIngestionFileType]?.icon;
}

export function getDataIngestionFileTypeText(dataIngestionFileType: string) {
  return dataIngestionFileTypeHelper?.[dataIngestionFileType]?.text;
}
