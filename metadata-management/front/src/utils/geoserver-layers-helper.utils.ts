const geoserverLayerCategoryHelper = {
  LIMITES_TERRITORIAIS: {
    text: "Limites territoriais",
  },
  EQUIPAMENTOS_PUBLICOS_CULTURA: {
    text: "Equipamentos Públicos - Cultura",
  },
  EQUIPAMENTOS_PUBLICOS_EDUCACAO: {
    text: "Equipamentos Públicos - Educação",
  },
};

export function getGeoserverLayerCategoryText(geoserverLayerCategory: string) {
  return geoserverLayerCategoryHelper?.[geoserverLayerCategory]?.text;
}
