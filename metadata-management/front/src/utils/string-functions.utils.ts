import { deburr } from "lodash";

export function cleanString(string: string) {
  return deburr(string).toLowerCase();
}
