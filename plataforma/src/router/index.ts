// Composables
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("@/layouts/LayoutDefault.vue"),
    children: [
      {
        path: "",
        name: "Home",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Home.vue"),
      },
    ],
  },
  {
    path: "/app",
    component: () => import("@/layouts/LayoutMap.vue"),
    children: [
      {
        path: "",
        name: "Maps",
        component: () =>
          import(/* webpackChunkName: "map" */ "@/pages/maps/Map.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
