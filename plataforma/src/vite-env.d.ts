/// <reference types="vite/client" />

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

interface ImportMetaEnv {
  readonly VITE_APP_TITLE: string;
  // geoserver
  readonly VITE_GEOSERVER_URL_WMS: string;
  readonly VITE_GEOSERVER_WORKSPACE: string;
  readonly VITE_GEOSERVER_LAYER_UFS: string;
  readonly VITE_GEOSERVER_LAYER_MUNICIPIOS: string;
  readonly VITE_GEOSERVER_LAYER_SETORES: string;
  readonly VITE_GEOSERVER_LAYER_EQUIPAMENTOS_ESCOLAS: string;
  // initial values
  readonly VITE_INITIAL_LONGITUDE: string;
  readonly VITE_INITIAL_LATITUDE: string;
  readonly VITE_INITIAL_MAP_ZOOM: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
