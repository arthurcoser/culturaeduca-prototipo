import { createApp } from 'vue/dist/vue.esm-bundler';
import App from './App.vue';

import { registerPlugins } from './plugins'

import './style.css'

const app = createApp(App);

registerPlugins(app);

app.mount("#app");
