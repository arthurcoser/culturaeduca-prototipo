import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: '/',
    component: () =>
      import(/* webpackChunkName: "home" */ "@/pages/Landing/LandingPage.vue"),
  },
  {
    path: '/app',
    component: () =>
      import(/* webpackChunkName: "home" */ "@/pages/Maps/Map.vue"),
  },
  {
    path: '/sobre',
    component: () =>
      import(/* webpackChunkName: "home" */ "@/pages/Sobre/Sobre.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes, // short for `routes: routes`
});

export default router;
