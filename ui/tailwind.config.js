/** @type {import('tailwindcss').Config} */
export default {
  /* Opções que evitam conflitos com vuetify */
  prefix: 'tw-',
  /* Fim das opções que evitam conflito com vuetify */
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./src/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

